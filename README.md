Assignment details 
- Requirements
  - API Server : pick any URL here
  - CI platform : Gitlab CI
  - BDD format : Cucumber/Gherkin
  - Framework: Java Serenity

  You need:
  - Automate the endpoints inside a working CI/CD pipeline to test the service
  - Cover at least 1 positive and 1 negative scenario
  - Write instructions in Readme file on how to install, run and write new tests
  - Do not forget about .gitignore
  - Document the interactions of end points
  - Upload your project to Gitlab and make sure that CI/CD is configured
  - set Html reporting tool with the test results of the CI/CD  test run
  - IMPORTANT!! provide us with a Gitlab link to your repository

- What is Senerity framework :
  Serenity is a Java-based library for test automation that wraps and extends WebDriver and JUnit functionality. 
  Serenity also wraps around BDD style tools like Cucumber and jBehave

- Technologies used : 
  - Java 11
  - Intellij IDE
  - Maven
  - Serenity framework

How to run :
 - Clone the repository from https://gitlab.com/g9848921789/openapitest
 - go to the root folder of the project (~cd openapitest)
 - run 'mvn clean install'. This command will install the required dependencies and run the test cases
 - We could see the result in openapitest/target/site/serenity/index.html
 - Result should be 100% success 
 - This project covers two tests
    - one is positive scenario where API will return the 200 as a response status code
    - another one is negative scenario where API will return 404 as a response status code
 